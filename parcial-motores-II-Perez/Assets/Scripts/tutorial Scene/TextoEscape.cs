using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextoEscape : MonoBehaviour
{
    public static bool activarEscape = false; 

    public Animator animator;

    private void Start()
    {
       // animator = GetComponent<Animator>(); 
    }

    private void Update()
    {
        if (activarEscape)
        {
            animator.SetBool("escape", true); 
            activarEscape = false; 
        }
    }
}
