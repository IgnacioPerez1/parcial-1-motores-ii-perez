using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialButton : MonoBehaviour
{
    public GameObject pared;
    public Material materiaB2;
    private Material originalMaterial;
    public GameObject colliderObject;

    private void Start()
    {
        originalMaterial = pared.GetComponent<MeshRenderer>().material;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("buttonPresser"))
        {
            pared.GetComponent<MeshRenderer>().material = materiaB2;
            colliderObject.SetActive(true);
        }
    }
}
