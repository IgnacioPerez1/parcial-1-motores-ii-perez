using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class savePositions : MonoBehaviour
{
    private List<Vector3> posicionesGuardadas = new List<Vector3>();
    private Collider colisionador;

    private void Start()
    {
        colisionador = GetComponent<Collider>();
    }

    private void Update()
    {
        if (!HayOverlapEnPosicion(transform.position))
        {
            posicionesGuardadas.Add(transform.position);
        }
    }

    private bool HayOverlapEnPosicion(Vector3 posicion)
    {
        Collider[] collidersEnOverlap = Physics.OverlapBox(posicion, colisionador.bounds.extents);

        return collidersEnOverlap.Length > 0;
    }
}