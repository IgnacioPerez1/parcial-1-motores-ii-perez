using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class superliminal : MonoBehaviour
{
    [Header("Components")]
    public Transform target;

    [Header("Parameters")]
    public LayerMask targetMask;
    public LayerMask ignoreTargetMask;
    public float offsetFactor;
    public float lerpSpeed = 5f;

    public GameObject miraObject;
    public Sprite agarrarObjetoSprite;
    public Sprite noHayObjetoAgarrableSprite;
    public Sprite objetoAgarradoSprite;
    public Sprite objetoRotandoSprite;
    public Sprite miraNormalSprite;

    private float originalDistance;
    private Collider originalCollider;
    private Vector3 originalScale;
    private Rigidbody targetRigidbody;
    private float newMass;

    private string tagOriginal;
    private string tagJugador;
    private int originalLayer;

    public AudioSource sonidoAgarrar;
    public AudioSource sonidoSoltar;

    public static bool estaRotando;

    private float rotationSpeed = 10f;
    private Vector3 rotationEuler;
    private Image miraImage;

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        tagJugador = "Untagged";
        target = null;
        miraImage = miraObject.GetComponent<Image>(); 
    }

    private void Update()
    {
        HandleInput();
        ResizeTarget();
        if (target == null) { UpdateMiraImage(); } 
        if (target != null && estaRotando)
        {
            RotateTarget();
        }
    }

    private void HandleInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (target == null)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity, targetMask))
                {
                    if (!Physics.Raycast(transform.position, transform.forward, out RaycastHit ignoreHit, Mathf.Infinity, ignoreTargetMask) || ignoreHit.distance > hit.distance)
                    {
                        sonidoAgarrar.Play();
                        target = hit.transform;
                        tagOriginal = target.tag;
                        target.tag = tagJugador;
                        targetRigidbody = target.GetComponent<Rigidbody>();
                        targetRigidbody.isKinematic = true;
                        originalDistance = Vector3.Distance(transform.position, target.position);
                        originalScale = target.localScale;
                        originalLayer = target.gameObject.layer;
                        target.gameObject.layer = LayerMask.NameToLayer("noTraspassable");
                        target.GetComponent<Collider>().enabled = false;
                        miraImage.sprite = objetoAgarradoSprite;
                        miraImage.enabled = true;
                    }
                }
            }
            else
            {
                target.GetComponent<Collider>().enabled = true;
                targetRigidbody.isKinematic = false;
                targetRigidbody.useGravity = true;
                target.gameObject.layer = originalLayer;
                target.tag = tagOriginal;
                target = null;
                tagOriginal = "Untagged";
                sonidoSoltar.Play();
                miraImage.sprite = miraNormalSprite; 
                estaRotando = false; 
            }
        }
        else if (Input.GetMouseButtonDown(1))
        {
            estaRotando = true;
            rotationEuler = target.rotation.eulerAngles;
            miraImage.sprite = objetoRotandoSprite;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            estaRotando = false;
            if (target != null)
            {
                miraImage.sprite = objetoAgarradoSprite;
            }
        }
    }

    private void ResizeTarget()
    {
        if (target == null)
        {
            return;
        }

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity, ignoreTargetMask))
        {
            Vector3 targetPosition = hit.point - transform.forward * offsetFactor * originalScale.x;
            target.position = Vector3.Lerp(target.position, targetPosition, Time.deltaTime * lerpSpeed);

            targetRigidbody.mass = 0.1f;
            float currentDistance = Vector3.Distance(transform.position, target.position);
            float scaleRatio = currentDistance / originalDistance;
            Vector3 newScale = originalScale * scaleRatio;

            float newMass = newScale.x * newScale.y * newScale.z;

            target.localScale = newScale;
            targetRigidbody.mass = newMass;
        }
    }

    private void RotateTarget()
    {
        float rotationX = Input.GetAxis("Mouse Y") * rotationSpeed;
        float rotationY = Input.GetAxis("Mouse X") * rotationSpeed;

        rotationEuler.x += rotationX;
        rotationEuler.y += rotationY;

        rotationEuler.x = Mathf.Clamp(rotationEuler.x, -90f, 90f);

        Quaternion targetRotation = Quaternion.Euler(rotationEuler);
        target.rotation = targetRotation;
    }

    private void UpdateMiraImage()
    {
        RaycastHit[] hits = new RaycastHit[1];
        int numHits = Physics.RaycastNonAlloc(transform.position, transform.forward, hits, Mathf.Infinity, targetMask);

        if (numHits > 0)
        {
            miraImage.sprite = agarrarObjetoSprite;
        }
        else if (target == null)
        {
            miraImage.sprite = noHayObjetoAgarrableSprite;
        }
    }
}
