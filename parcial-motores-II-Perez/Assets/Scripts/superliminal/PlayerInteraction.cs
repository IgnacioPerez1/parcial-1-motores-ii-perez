using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public float interactionRange = 2f;
    public Camera mainCamera;
    public LayerMask targetLayer;

    private Transform grabbedObject;
    private Transform previousParent;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (grabbedObject == null)
            {
                RaycastHit hit;
                if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, interactionRange, targetLayer))
                {
                    grabbedObject = hit.transform;
                    previousParent = grabbedObject.parent;
                    grabbedObject.SetParent(transform);
                }
            }
            else
            {
                grabbedObject.SetParent(previousParent);
                grabbedObject = null;
            }
        }
    }
}
