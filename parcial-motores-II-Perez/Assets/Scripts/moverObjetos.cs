using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moverObjetos : MonoBehaviour
{
    private GameObject grabbedObject;
    private bool isGrabbing;

    private Camera mainCamera;
    public float grabDistance = 3f;

    private void Start()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (!isGrabbing)
            {
                TryGrabObject();
            }
            else
            {
                DropObject();
            }
        }

        if (isGrabbing)
        {
            MoveObject();
        }
    }

    private void TryGrabObject()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, grabDistance))
        {
            if (hit.collider.CompareTag("Grabbable"))
            {
                grabbedObject = hit.collider.gameObject;
                grabbedObject.GetComponent<Rigidbody>().isKinematic = true;
                grabbedObject.transform.SetParent(mainCamera.transform);
                isGrabbing = true;
            }
        }
    }

    private void MoveObject()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, grabDistance);
        Vector3 objectPosition = mainCamera.ScreenToWorldPoint(mousePosition);
        grabbedObject.transform.position = objectPosition;
    }

    private void DropObject()
    {
        grabbedObject.GetComponent<Rigidbody>().isKinematic = false;
        grabbedObject.transform.SetParent(null);
        grabbedObject = null;
        isGrabbing = false;
    }
}
