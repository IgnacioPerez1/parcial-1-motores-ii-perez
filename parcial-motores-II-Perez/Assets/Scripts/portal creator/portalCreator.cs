using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portalCreator : MonoBehaviour
{
    public GameObject portalA;
    public GameObject portalB;
    public Camera camJugador;
    public float maxDistance = 10f;
    public LayerMask targetMask;


    private void Start()
    {
      //  camJugador = GameObject.Find("Player").GetComponent<Camera>();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            PortalA();
        }
        if (Input.GetMouseButtonDown(1))
        {
            PortalB();
        }
    }

    private void PortalA()
    {
        RaycastHit hit;
        if (Physics.Raycast(camJugador.transform.position, camJugador.transform.forward, out hit, Mathf.Infinity, targetMask))
        {
            portalA.transform.position = hit.point;
            Quaternion portalRotation = Quaternion.LookRotation(camJugador.transform.forward);
            portalRotation.eulerAngles = new Vector3(0f, portalRotation.eulerAngles.y, 0f);
            portalA.transform.rotation = portalRotation;
        }
    }

    private void PortalB()
    {
        RaycastHit hit;
        if (Physics.Raycast(camJugador.transform.position, camJugador.transform.forward, out hit, Mathf.Infinity, targetMask))
        {
            portalB.transform.position = hit.point;
            Quaternion portalRotation = Quaternion.LookRotation(camJugador.transform.forward);
            portalRotation.eulerAngles = new Vector3(0f, portalRotation.eulerAngles.y, 0f);
            portalB.transform.rotation = portalRotation;
        }
    }
}
