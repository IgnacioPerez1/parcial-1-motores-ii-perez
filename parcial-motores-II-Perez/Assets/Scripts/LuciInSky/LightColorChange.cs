using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightColorChange : MonoBehaviour
{
    private Light directionalLight;

    private void Start()
    {
        directionalLight = GetComponent<Light>();

        // Inicia la repetición del cambio de color cada 0.5 segundos
        InvokeRepeating("ChangeLightColor", 0f, 0.5f);
    }

    private void ChangeLightColor()
    {
        // Genera un color aleatorio
        Color randomColor = new Color(Random.value, Random.value, Random.value);

        // Asigna el color aleatorio a la luz
        directionalLight.color = randomColor;
    }
}
