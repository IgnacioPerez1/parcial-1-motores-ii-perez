using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rendomLuz : MonoBehaviour
{
    public Renderer rend;
    public float changeInterval = 0.5f;

    private float timer = 0f;

    private void Start()
    {
        rend = GetComponent<Renderer>();
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= changeInterval)
        {
            timer = 0f;

            
            Color randomColor = new Color(Random.value, Random.value, Random.value);

            
            rend.material.SetColor("_EmissionColor", randomColor);
        }
    }
}
