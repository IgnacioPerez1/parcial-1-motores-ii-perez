using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 5f;
    public Camera mainCamera;
    public float wallSeparationForce = 5f;
    private Rigidbody rb;
    public float jumpForce;
    public float gravity = 20f;
    public bool isGrounded;
    public float maxLookDownAngle = -80f;  // �ngulo m�ximo hacia abajo

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        isGrounded = true;
    }

    private void Update()
    {
        // Verificar si est� rotando en superliminal
        bool estaRotando = superliminal.estaRotando;

        if (!estaRotando)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 cameraForward = mainCamera.transform.forward;
            Vector3 cameraRight = mainCamera.transform.right;
            cameraForward.y = 0f;
            cameraRight.y = 0f;
            cameraForward.Normalize();
            cameraRight.Normalize();

            Vector3 movement = (cameraForward * moveVertical + cameraRight * moveHorizontal) * speed;

            rb.velocity = new Vector3(movement.x, rb.velocity.y, movement.z);


            if (Input.GetKeyDown(KeyCode.R))
            {
                int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
                SceneManager.LoadScene(currentSceneIndex);
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
            {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                isGrounded = false;
            }
            ApplyGravity();

            // Obtener la rotaci�n de la c�mara en el eje X
            float cameraRotationX = mainCamera.transform.eulerAngles.x;

            // Restringir la rotaci�n hacia abajo
            if (cameraRotationX > 180f)
            {
                cameraRotationX -= 360f;
            }
            cameraRotationX = Mathf.Clamp(cameraRotationX, maxLookDownAngle, 80f); // �ngulo m�ximo hacia arriba

            // Aplicar la rotaci�n limitada a la c�mara
            mainCamera.transform.eulerAngles = new Vector3(cameraRotationX, mainCamera.transform.eulerAngles.y, mainCamera.transform.eulerAngles.z);
        }
    }

    private void FixedUpdate()
    {

    }

    void ApplyGravity()
    {
        rb.AddForce(Vector3.down * gravity * Time.fixedDeltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ground"))
        {
            isGrounded = true;

        }
        if (collision.transform.CompareTag("escape"))
        {
            TextoEscape.activarEscape = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("SceneChanger"))
        {
            SceneManager.LoadScene("Scene endless");
        }
        if (other.gameObject.layer == LayerMask.NameToLayer("SceneChangerEnd"))
        {
            SceneManager.LoadScene("LucyInSky");
        }
        if (other.transform.CompareTag("AbrirPuerta"))
        {
            puerta.AbrirPuerta = true;
            Debug.Log("abriendo puerta");
        }
        if (other.transform.CompareTag("escape"))
        {
            TextoEscape.activarEscape = true;
        }
    }
}
