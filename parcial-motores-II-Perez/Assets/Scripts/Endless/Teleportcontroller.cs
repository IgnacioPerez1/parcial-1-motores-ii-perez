using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleportcontroller : MonoBehaviour
{
    public Transform teleport;
    public Transform teleport2;
    public Transform teleport3;

    public Camera camaraFinal;
    public Camera camaraJugador;

    private void Awake()
    {
        camaraFinal.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("teleport"))
        {
            this.gameObject.transform.position = teleport.position;
        }
        if (other.gameObject.CompareTag("teleport2"))
        {
            this.gameObject.transform.position = teleport2.position;
            camaraJugador.gameObject.SetActive(false);
            camaraFinal.gameObject.SetActive(true);
        }
        if (other.gameObject.CompareTag("teleport3"))
        {
            this.gameObject.transform.position = teleport3.position;
        }
    }
}
