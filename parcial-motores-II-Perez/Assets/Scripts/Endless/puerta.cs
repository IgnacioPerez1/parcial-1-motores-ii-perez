using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puerta : MonoBehaviour
{
    public static bool AbrirPuerta;
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (AbrirPuerta)
        {
            animator.SetBool("abrir puerta", true);
        }
    }
}