using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float sensitivity = 2f;
    public Transform playerTransform;
    public float maxVerticalAngle = 90f;
    public float minVerticalAngle = -90f;

    private float mouseX;
    private float mouseY;
    private float rotationX = 0f;
    private float rotationY = 0f;

    private void Update()
    {
        bool estaRotando = superliminal.estaRotando;

        if (!estaRotando)
        {
            mouseX = Input.GetAxis("Mouse X") * sensitivity;
            mouseY = Input.GetAxis("Mouse Y") * sensitivity;

            rotationY += mouseX;
            rotationX -= mouseY;

            rotationX = Mathf.Clamp(rotationX, minVerticalAngle, maxVerticalAngle);

            transform.rotation = Quaternion.Euler(rotationX, rotationY, 0f);
            playerTransform.rotation = Quaternion.Euler(0f, rotationY, 0f);
        }
    }
}